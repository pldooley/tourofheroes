import { Component, OnInit } from '@angular/core';
import { Range } from 'src/app/state/range/range';
import { RangeService } from 'src/app/state/range/range.service';
import { EventHttpService } from 'src/app/http/event/event.http.service';
import { EventsService } from 'src/app/state/events/events.service';
import { Event } from 'src/app/state/event/event';


@Component({
  selector: 'app-event-search',
  templateUrl: './event-search.component.html',
  styleUrls: ['./event-search.component.css']
})
export class EventSearchComponent implements OnInit {

  option; number = 1;

  sub: any;

  range: Range = null;

  constructor(
    private rangeService: RangeService,
    private httpService: EventHttpService,
    private eventsService: EventsService)
  {

  }

  ngOnInit()
  {
    this.sub = this.rangeService.state$
      .subscribe((range: Range) => this.load(range) )
  }

  ngOnDestroy()
  {
    this.sub.unsubscribe();
  }

  load(range: Range)
  {
    console.log("EventSearchComponent:load() called.")

    if (!range)
    {
      return;
    }
    this.range = range;

    this.httpService.search(range)
      .subscribe((events: Event[]) => {
          this.store(events);
      })

  }

  store(data: Event[])
  {
    console.log("EventSearchComponent:store() called.")

    this.eventsService.announce(data);
  }


}
