import { Component, OnInit } from '@angular/core';
import { Config } from 'src/app/state/config/config';
import { ConfigHttpService } from 'src/app/http/config/config.http.service';
import { ConfigService } from 'src/app/state/config/config.service';

@Component({
  selector: 'config-http',
  template: '<h1>config-http</h1>'
})
export class ConfigHttpComponent implements OnInit
{

  config: Config;
  headers;

  constructor(
    private service: ConfigService,
    private httpService: ConfigHttpService )
  {
  }

  ngOnInit()
  {
    this.getConfig();
  }

  getConfig()
  {
    this.httpService.get()
      .subscribe((data: Config) => {
        this.config = {...data};
        this.store();
      });
  }

  showConfigResponse()
  {
    this.httpService.getResponse()
      .subscribe(resp => {
        const keys = resp.headers.keys();
        this.headers = keys.map(key =>`${key}: ${resp.headers.get(key)}`);
        this.config = { ... resp.body};
      });
  }

  store()
  {
    console.log("ConfigComponent::store() called");
    if (!this.config)
    {
      console.error("config is invalid");
      return;
    }


    this.service.announce(this.config);

  }
}
