import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Event } from 'src/app/state/event/event';
import { MessageService } from 'src/app/message.service';
import { EventUrlService } from 'src/app/state/event/event.url.service';
import { Range }  from 'src/app/state/range/range';


@Injectable({
  providedIn: 'root'
})
export class EventHttpService {

  private httpOptions =
  {
    headers: new HttpHeaders({'Content-Type' : 'Application/json'})
  }

  constructor(
    private messageService: MessageService,
    private http: HttpClient,
    private eventUrlService: EventUrlService )
  {
    console.log("EventHttpService:constructor() called.");
  }

  add(event: Event): Observable<Event>
  {
    console.log("EventHttpService:add() called.");

    let sUrl = this.eventUrlService.getValue();
    console.log(`EventHttpService:add() sUrl is ${sUrl}`);

    return this.http.post<Event>(sUrl, event, this.httpOptions)
      .pipe(
        tap((newEvent: Event) => this.log(`added event w/ id=${newEvent.id}`)),
        catchError(this.handleError<Event>('addEvent'))
      )
  }

  delete(id: string): Observable<any>
  {
    console.log("EventHttpService:delete() called.");

    let sApi = this.eventUrlService.getValue();
    const sUrl = `${sApi}/${id}`;
    console.log(`EventHttpService:delete() url is ${sUrl}`);

    return this.http.delete(sUrl)
      .pipe(
        tap( () => this.log(`deleted event w/ id=${id}`)),
        catchError(this.handleError<Event>('deleteEvent'))
      )
  }

  get(id: number): Observable<Event>
  {
    console.log("EventHttpService:get() called.");

    this.log(`EventHttpService: fetch event id: ${id}`);

    let sApi = this.eventUrlService.getValue();
    const sUrl = `${sApi}/${id}`;
    console.log(`EventHttpService:get() url is ${sUrl}`);

    return this.http.get<Event>(sUrl)
      .pipe(
        tap(_ => this.log(`fetched event id=${id}`)),
        catchError(this.handleError<Event>(`getEvent id=${id}`))
      )
  }

  search(range: Range): Observable<Event[]>
  {
    console.log("EventHttpService:search() called.");

    let sUrl = this.eventUrlService.getValue();

    if (!range)
    {
      console.log(`EventHttpService:search() range is invalid`);
      return;
    }

    let sStart = range.start;
    let sEnd = range.end;

    const url = `${sUrl}/${sStart}/${sEnd}`;
    console.log(`EventHttpService:search() url is ${url}`);

    return this.http.get<Event[]>(url)
      .pipe(
        tap(_=> this.log(`found events matching "${sStart}"`)),
        catchError(this.handleError<Event[]>('searchEvents', []))
      )
  }

  update(event: Event): Observable<any>
  {
    console.log("EventHttpService:update() called.");

    this.log(`EventHttpService: update event ${event}`);

    let sUrl = this.eventUrlService.getValue();
    console.log(`EventHttpService:update() sUrl is ${sUrl}`);

    return this.http.put(sUrl, event, this.httpOptions)
      .pipe(
        tap(_ => this.log(`updated event id=${event.id}`)),
        catchError(this.handleError<Event>(`updateEvent ${event}`))
      )
  }

  private log(message: string)
  {
    this.messageService.add(message);
  }

  private handleError<T>(operation = 'operation', result?: T)
  {
    return (error: any): Observable<T> => {

      console.error(error);

      this.log(`${operation} failed: ${error.message}`)

      return of(result as T);
    }
  }

}
