import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { EventDetail } from 'src/app/state/eventDetail/eventDetail';
import { MessageService } from 'src/app/message.service';
import { EventDetailUrlService } from 'src/app/state/eventDetail/eventDetail.url.service';

@Injectable({
  providedIn: 'root'
})
export class EventDetailHttpService {

  private httpOptions =
  {
    headers: new HttpHeaders({'Content-Type' : 'Application/json'})
  }

  constructor(
    private messageService: MessageService,
    private http: HttpClient,
    private urlService: EventDetailUrlService )
  {
    console.log("EventDetailHttpService:constructor() called.");
  }

  add(detail: EventDetail): Observable<EventDetail>
  {
    console.log("EventDetailHttpService:add() called.");

    let sUrl = this.urlService.getValue();
    console.log(`EventDetailHttpService:add() sUrl is ${sUrl}`);

    return this.http.post<EventDetail>(sUrl, detail, this.httpOptions)
      .pipe(
        tap((detail: EventDetail) => this.log(`added detail w/ id=${detail.eventId}`)),
        catchError(this.handleError<EventDetail>('add'))
      )
  }

  delete(id: number): Observable<any>
  {
    console.log("EventDetailHttpService:delete() called.");

    let sApi = this.urlService.getValue();
    const sUrl = `${sApi}/${id}`;
    console.log(`EventHttpService:delete() url is ${sUrl}`);

    return this.http.delete(sUrl)
      .pipe(
        tap( () => this.log(`deleted event w/ id=${id}`)),
        catchError(this.handleError<Event>('delete'))
      )
  }

  get(id: number): Observable<EventDetail>
  {
    console.log("EventDetailHttpService:get() called.");

    this.log(`EventDetailHttpService: fetch event id: ${id}`);

    let sApi = this.urlService.getValue();
    const sUrl = `${sApi}/${id}`;
    console.log(`EventDetailHttpService:get() url is ${sUrl}`);

    return this.http.get<EventDetail>(sUrl)
      .pipe(
        tap(_ => this.log(`fetched event id=${id}`)),
        catchError(this.handleError<EventDetail>(`get id=${id}`))
      )
  }

  update(detail: EventDetail): Observable<any>
  {
    console.log("EventDetailHttpService:update() called.");

    this.log(`EventDetailHttpService: update detail ${detail}`);

    let sUrl = this.urlService.getValue();
    console.log(`EventDetailHttpService:update() sUrl is ${sUrl}`);

    return this.http.put(sUrl, detail, this.httpOptions)
      .pipe(
        tap(_ => this.log(`updated detail id=${detail.eventId}`)),
        catchError(this.handleError<EventDetail>(`update ${detail}`))
      )
  }

  private log(message: string)
  {
    this.messageService.add(message);
  }

  private handleError<T>(operation = 'operation', result?: T)
  {
    return (error: any): Observable<T> => {

      console.error(error);

      this.log(`${operation} failed: ${error.message}`)

      return of(result as T);
    }
  }

}
