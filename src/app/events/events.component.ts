import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Event } from 'src/app/state/event/event';
import { EventHttpService } from 'src/app/http/event/event.http.service';
import { EventUrlService } from 'src/app/state/event/event.url.service';
import { SelectedService } from 'src/app/state/selected/selected.service';
import { EventsService } from 'src/app/state/events/events.service';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {


  events: Event[];

  subscription: any;
  subscriptionData: any;

  gridApi: any;
  gridColumnApi: any;

  columnDefs =
    [
      {headerName: 'Type', field: 'eventType'},
      {headerName: 'Summary', field: 'eventSummary'},
      {headerName: 'Date', field: 'eventDate'},
      {headerName: 'Delete', cellRenderer: (data)=>
        {
          return`<i class="icon-trash cell-btn-remove"` +
          ` title="Delete this record"` +
          ` ng-click="deleteRecord(data,'+params.node.id+')">`
        }
      }
    ];

  rowData: any;

  gridOptions =
    {
      defaultColDef: {
        editable: true
      }
    }

    rowSelection="multiple";

  constructor(
    private httpService: EventHttpService,
    private urlService: EventUrlService,
    private router: Router,
    private selectedService: SelectedService,
    private eventsService: EventsService )
  {
  }

  ngOnInit()
  {
    console.log("EventsComponent::ngOnInit() called.");
    this.subscription = this.eventsService.state$
      .subscribe(data => {
          this.rowData = data;
       });
  }

  ngOnDestroy()
  {
    console.log("EventsComponent::ngOnDestroy() called.");

    this.subscriptionData.unsubscribe();
  }

  onGridReady(params)
  {
    console.log("EventsComponent::onGridReady() called.");

    if (!params)
    {
      console.error("EventsComponent::onGridReady() params is invalid.");
      return;
    }
    this.gridApi=params.api;
    this.gridColumnApi=params.columnApi;
  }

  onAddRow()
  {
    console.log("EventsComponent::onGridReady() called.");

    let event = this.createNewRowData();
    let res = this.gridApi.updateRowData({ add: [event] });

    let data: Event = {
      'id': 0,
      'eventType':res.add[0].data.event_type,
      'eventSummary':res.add[0].data.event_summary,
      'eventDate': res.add[0].data.event_date };

    console.log(data)

    this.httpService.add(data);
  }

  onRemoveSelected()
  {
    console.log("EventsComponent::onRemoveSelected() called.");

    let selectedData = this.gridApi.getSelectedRows();
    let res = this.gridApi.updateRowData({ remove: selectedData });
    console.log(res.remove[0].data.id);
    let id = res.remove[0].data.id;

    this.httpService.delete(id);
  }

  onUpdate()
  {
    console.log("EventsComponent::onUpdate() called.");

    let selectedData = this.gridApi.getSelectedRows();
    let Event = selectedData[0];

    this.selectedService.announce(Event);

    let id = selectedData[0].id;

    this.router.navigate(['/detail/' + id] );
  }

  createNewRowData() {
    let newData = {
      type: "asdfasfdasf",
      summary: "alsjkfaksjf kajsfdalksjf alksfjal;sfk",
      date: "20110323"
    };
    return newData;
  }



}
