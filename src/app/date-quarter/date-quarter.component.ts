import { Component, OnInit } from '@angular/core';
import { MatDatepicker } from "@angular/material/datepicker"
import { Moment } from "moment"
import { FormControl } from "@angular/forms"
import { RangeService } from 'src/app/state/range/range.service';
import { MathService } from 'src/app/service/math.service';
import { DateRangeService } from 'src/app/service/dateRange.service';
import { Range } from 'src/app/state/range/range';

@Component({
  selector: 'app-date-quarter',
  templateUrl: './date-quarter.component.html',
  styleUrls: ['./date-quarter.component.css']
})
export class DateQuarterComponent implements OnInit {

  date = new FormControl('');
  quarter: number;
  year: number;

  constructor(
    private rangeService: RangeService,
    private mathService: MathService,
    private dateRangeService: DateRangeService )
  {

  }

  ngOnInit()
  {
  }

  onRange()
  {
    console.log("DateMonthComponent:onRange() called.")

    if (!this.quarter)
    {
      console.log("DateMonthComponent:onRange() quarter invalid.")
      return;
    }

    if (!this.year)
    {
      console.log("DateMonthComponent:onRange() year invalid.")
      return;
    }

    const range: Range = this.dateRangeService.quarterRange(+this.quarter, +this.year);
    if (!range)
    {
      console.log("DateMonthComponent:onRange() range invalid.")
      return;
    }

    this.rangeService.announce(range);
  }

  chosenYearHandler(date: Moment)
  {
    this.year = date.year();
    console.log("year is ", this.year);
  }

  chosenMonthHandler(date: Moment, datepicker: MatDatepicker<Moment>)
  {
    this.quarter = date.quarter();
    console.log("quarter is ", this.quarter);
    this.date.setValue(`${this.quarter}/${this.year}`)

    console.log(this.date.value);
    datepicker.close();
  }


}
