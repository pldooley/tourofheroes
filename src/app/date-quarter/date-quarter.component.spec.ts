import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateQuarterComponent } from './date-quarter.component';

describe('DateQuarterComponent', () => {
  let component: DateQuarterComponent;
  let fixture: ComponentFixture<DateQuarterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateQuarterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateQuarterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
