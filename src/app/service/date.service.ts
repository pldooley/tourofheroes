import { Injectable } from '@angular/core';
import * as moment from "moment"

@Injectable({
  providedIn: 'root'
})
export class DateService
{

  public quarterToMonth(quarter: number): number
  {
    console.log("DateService:quarterToMonth() called.");

    if (quarter<=0)
    {
      console.error("DateService):quarterToMonth() quarter invalid.");
      return 0;
    }
    console.log("DateService):quarterToMonth() quarter is ", quarter);

    let nMonth = quarter*3 - 2;
    console.log("DateService:quarterToMonth() nMonth is." + nMonth);
    return nMonth;
  }

  public firstDayOfWeek(week, year)
  {
    if (typeof year !== 'undefined')
    {
      year = (new Date()).getFullYear();
    }
    let date       = this.firstWeekOfYear(year);
    let weekTime   = this.weeksToMilliseconds(week);
    let targetTime = date.getTime() + weekTime - 86400000;
    let result = new Date(targetTime)

    return result;
  }

  private weeksToMilliseconds(weeks)
  {
    return 1000 * 60 * 60 * 24 * 7 * (weeks - 1);
  }

  private firstWeekOfYear(year)
  {
    let date = new Date();
    date = this.firstDayOfYear(date,year);
    date = this.firstWeekday(date);
    return date;
  }

  private firstDayOfYear(date, year)
  {
    date.setYear(year);
    date.setDate(1);
    date.setMonth(0);
    date.setHours(0);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
  }

  private firstWeekday(date)
  {
    let day = date.getDay();
    day = (day === 0) ? 7 : day;
    if (day > 3)
    {
      let remaining = 8 - day;
      let target = remaining + 1;
      date.setDate(target);
    }
    return date;
  }

}
