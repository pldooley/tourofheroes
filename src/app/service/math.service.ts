import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MathService
{

  public to2digit(n: number)
  {
    return ('00' + n).slice(-2);
  };
}
