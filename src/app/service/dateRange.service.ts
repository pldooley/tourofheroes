import { Injectable } from '@angular/core';
import * as moment from "moment"
import { DateService } from './date.service';
import { Range } from 'src/app/state/range/range';

@Injectable({
  providedIn: 'root'
})
export class DateRangeService
{

  constructor(
    private dateService: DateService  )
  {
    console.log("DateRangeService:constructor() called.");
  }

  public quarterRange(nQuarter: number, nYear: number): Range
  {
    console.log("DateRangeService:quarterRange() called.");

    if (!nQuarter)
    {
      console.error("DateRangeService:quarterRange() nQuarter invalid.");
      return null;
    }

    if (!nYear)
    {
      console.error("DateRangeService:quarterRange() nYear invalid.");
      return null;
    }

    let nMonth = this.dateService.quarterToMonth(nQuarter);
    if (!nMonth)
    {
      console.error("DateRangeService:quarterRange() nMonth invalid.");
      return null;
    }
    console.log("DateService:quarterToMonth() nMonth is." + nMonth);

    let start = moment();
    start.set("year", nYear).set("month", nMonth).set("day", 1);
    let sStart: String = start.format('YYYYMMDD');
    console.log("DateRangeService:onRange() sStart is." + sStart);

    start.add(3, "months").subtract(1, "day");
    let sEnd:String = start.format('YYYYMMDD');
    console.log("DateRangeService:onRange() sEnd is." + sEnd);

    let range: Range = {
      start: sStart,
      end: sEnd
    }

    return range;
  }


  public weekRange(nWeek: number, nYear: number)
  {
    console.log("DateRangeService:weekRange() called.")

    if (!nWeek)
    {
      console.log("DateRangeService:weekRange() nWeek invalid.")
      return;
    }
    console.log("DateRangeService:weekRange() nWeek is." + nWeek);

    if (!nYear)
    {
      console.log("DateRangeService:weekRange() nYear invalid.")
      return;
    }
    console.log("DateRangeService:weekRange() nYear is." + nYear);

    let date: Date = this.dateService.firstDayOfWeek(nWeek, nYear);
    if (!date)
    {
      console.log("DateRangeService:weekRange() date invalid.")
      return;
    }

    let m = moment(date);
    let sStart: String = m.format('YYYYMMDD');
    console.log("DateRangeService:weekRange() sStart is." + sStart);

    m.add(6, "days");
    let sEnd: String = m.format("YYYYMMDD");
    console.log("DateRangeService:weekRange() sEnd is." + sEnd);

    let range: Range = {
      start: sStart,
      end: sEnd
    }

    return range;
  }

}
