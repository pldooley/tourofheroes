import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';
import { FormControl } from "@angular/forms"
import { RangeService } from 'src/app/state/range/range.service';
import { Range } from 'src/app/state/range/range';
import { DateRangeService } from 'src/app/service/dateRange.service';

@Component({
  selector: 'app-date-week',
  templateUrl: './date-week.component.html',
  styleUrls: ['./date-week.component.css']
})
export class DateWeekComponent implements OnInit
{

  date = new FormControl(Date());

  constructor(
    private rangeService: RangeService,
    private dateService: DateRangeService )
  {

  }

  ngOnInit()
  {
  }

  onRange()
  {
    console.log("DateWeekComponent:onRange() called.")

    let year;
    let week;
    [year, week] = this.date.value.split("-W");

    let range: Range = this.dateService.weekRange(+year, +week);
    if (!range)
    {
      console.log("DateWeekComponent:onRange() range invalid..")
      return;
    }

    this.rangeService.announce(range);
  }


}
