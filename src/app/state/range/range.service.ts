import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Range } from './range';


@Injectable({
  providedIn: 'root'
})
export class RangeService {

  private announcedSource = new BehaviorSubject<Range>(null);

  state$ = this.announcedSource.asObservable();

  announce(state: Range)
  {
    console.log("searchService:announce() called");
    console.log(`searchService`);
    this.announcedSource.next(state);
  }

  getValue(): Range
  {
    return this.announcedSource.getValue();
  }

}
