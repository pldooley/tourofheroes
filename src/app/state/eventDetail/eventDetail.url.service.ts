import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventDetailUrlService {

  private announcedSource = new BehaviorSubject<string>('');

  state$ = this.announcedSource.asObservable();

  announce(state: string)
  {
    console.log("EventDetailUrlService:announce() called");
    console.log(`EventDetailUrlService:announce() state is ${state}`);
    this.announcedSource.next(state);
  }

  getValue()
  {
    return this.announcedSource.getValue();
  }

}
