import { Component, OnInit } from '@angular/core';

import { EventDetailUrlService } from 'src/app/state/eventDetail/eventDetail.url.service';
import { Config } from 'src/app/state/config/config';
import { ConfigService } from 'src/app/state/config/config.service';


@Component({
  selector: 'event-detail-url',
  template: '<h1>eventDataUrl</h1>'
})
export class EventDetailUrlComponent implements OnInit
{

  config: Config;
  headers;

  constructor(
    private configService: ConfigService,
    private urlService: EventDetailUrlService )
  {
    console.log("EventDetailUrlComponent::constructor() called");
  }

  ngOnInit()
  {
    console.log("EventDetailUrlComponent::ngOnInit() called");
    this.configService.state$
      .subscribe(((data: Config) => {
        this.config = {... data};
        this.store();
      }));
  }

  store()
  {
    console.log("EventDetailUrlComponent::store() called");
    if (!this.config)
    {
      console.error("config is invalid");
      return;
    }

    const sUrl = this.config.EventDetailUrl;
    if (!sUrl)
    {
      console.error("EventDetailUrlComponent::store() sUrl is invalid.");
      return;
    }
    console.info(`EventDetailUrlComponent::store() sUrl is: ${sUrl}`)

    this.urlService.announce(sUrl);
  }
}
