export class Event {
  id: number;
  eventDate: string;
  eventType: string;
  eventSummary: string;
}
