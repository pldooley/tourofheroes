import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EventUrlService {

  private announcedSource = new BehaviorSubject<string>('');

  state$ = this.announcedSource.asObservable();

  announce(state: string)
  {
    console.log("EventUrlService:announce() called");
    console.log(`EventUrlService:announce() state is ${state}`);
    this.announcedSource.next(state);
  }

  getValue()
  {
    return this.announcedSource.getValue();
  }

}
