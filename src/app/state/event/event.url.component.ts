import { Component, OnInit } from '@angular/core';

import { EventUrlService } from 'src/app/state/event/event.url.service';
import { Config } from 'src/app/state/config/config';
import { ConfigService } from 'src/app/state/config/config.service';


@Component({
  selector: 'event-url',
  template: '<h1>eventUrl</h1>'
})
export class EventUrlComponent implements OnInit
{

  config: Config;
  headers;

  constructor(
    private configService: ConfigService,
    private urlService: EventUrlService )
  {
  }

  ngOnInit()
  {
    this.configService.state$
      .subscribe(((data: Config) => {
        this.config = {... data};
        this.store();
      }));
  }

  store()
  {
    console.log("ConfigComponent::store() called");
    if (!this.config)
    {
      console.error("config is invalid");
      return;
    }

    const sUrl = this.config.EventsUrl;
    if (!sUrl)
    {
      console.error("Config.url.component::store() sUrl is invalid.");
      return;
    }
    console.info(`Config.url.component::store() sUrl is: ${sUrl}`)

    this.urlService.announce(sUrl);
  }
}
