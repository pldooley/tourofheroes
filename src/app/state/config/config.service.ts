import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Config } from 'src/app/state/config/config';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private announcedSource = new BehaviorSubject<Config>(null);

  state$ = this.announcedSource.asObservable();

  announce(state: Config)
  {
    console.log("ConfigService:announce() called");
    console.log(`ConfigService:announce() state is ${state}`);
    this.announcedSource.next(state);
  }

  getValue()
  {
    return this.announcedSource.getValue();
  }

}
