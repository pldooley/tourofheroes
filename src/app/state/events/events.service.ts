import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Event } from 'src/app/state/event/event';


@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private announcedSource = new BehaviorSubject<Event[]>([]);

  state$ = this.announcedSource.asObservable();

  announce(state: Event[])
  {
    console.log("EventUrlService:announce() called");
    console.log(`EventUrlService:announce() state is ${state}`);
    this.announcedSource.next(state);
  }

  getValue()
  {
    return this.announcedSource.getValue();
  }

}
