import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Event } from 'src/app/state/event/event';

@Injectable({
  providedIn: 'root'
})
export class SelectedService {

  private announcedSource = new BehaviorSubject<Event>(null);

  state$ = this.announcedSource.asObservable();

  announce(state: Event)
  {
    console.log("SelectedService:announce() called");
    console.log(`SelectedService:announce() state is ${state}`);
    this.announcedSource.next(state);
  }

  getValue()
  {
    return this.announcedSource.getValue();
  }

}
