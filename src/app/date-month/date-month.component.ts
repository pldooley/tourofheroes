import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms"
import * as moment from 'moment';
import {DateAdapter} from "@angular/material/core"
import {MomentDateAdapter} from "@angular/material-moment-adapter"
import {MAT_DATE_FORMATS} from "@angular/material/core"
import {MAT_DATE_LOCALE} from "@angular/material/core"
import {Moment} from "moment"
import {MatDatepicker} from '@angular/material/datepicker';
import { RangeService } from 'src/app/state/range/range.service';
import { Range } from 'src/app/state/range/range';


export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: "MM/YYYY",
    monthYearLabel: 'MMM YYYY',
    dateAllyLabel: 'LL',
    monthYearAllyLabel: 'MMM YYYY'
  }
}

@Component({
  selector: 'app-date-month',
  templateUrl: './date-month.component.html',
  styleUrls: ['./date-month.component.css'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS}
  ]
})
export class DateMonthComponent implements OnInit {

  date = new FormControl(moment());

  constructor(
    private rangeService: RangeService )
  {

  }

  ngOnInit() {
  }

  onRange()
  {
    console.log("DateMonthComponent:onRange() called.")
    let date: Date = new Date(this.date.value);
    date.setDate(1);
    let m = moment(date);
    let sStart: String = m.format('YYYYMMDD');
    console.log("DateMonthComponent:onRange() sStart is." + sStart);

    let end = m.add(1, "month").subtract(1, "day");
    let sEnd:String = m.format("YYYYMMDD");
    console.log("DateMonthComponent:onRange() sEnd is." + sEnd);

    let range: Range = {
      start: sStart,
      end: sEnd
    }

    this.rangeService.announce(range);
  }

  chosenYearHandler(normalizedYear: Moment)
  {
    const val = this.date.value;
    val.year(normalizedYear.year());
    this.date.setValue(val);
  }

  chosenMonthHandler(normalizedMonth: Moment, datepicker: MatDatepicker<Moment>)
  {
    const val = this.date.value;
    val.month(normalizedMonth.month());
    this.date.setValue(val);
    datepicker.close();
  }

}
