import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateMonthComponent } from './date-month.component';

describe('DateMonthComponent', () => {
  let component: DateMonthComponent;
  let fixture: ComponentFixture<DateMonthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateMonthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateMonthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
