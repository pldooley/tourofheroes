import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Event } from 'src/app/state/event/event';
import { EventDetail } from 'src/app/state/eventDetail/eventDetail';
import { EventDetailHttpService } from 'src/app/http/eventDetail/eventDetail.http.service';
import { SelectedService } from 'src/app/state/selected/selected.service';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

  detail: EventDetail;
  event: Event;

  subDetail: any;
  subEvent: any;

  constructor(
    private route: ActivatedRoute,
    private httpService: EventDetailHttpService,
    private location: Location,
    private selectedService: SelectedService )
  {

  }

  ngOnInit()
  {
    this.getDetail();
    this.getEvent();
  }

  ngOnDestroy()
  {
    this.subDetail.unsubscribe();
    this.subEvent.unsubscribe();
  }

  getDetail(): void
  {
    const id = +this.route.snapshot.paramMap.get('id');
    this.subDetail = this.httpService.get(id)
      .subscribe(detail => {
        console.log(detail);
        this.detail = detail
      })
  }

  getEvent(): void
  {
    this.subEvent = this.selectedService.state$
      .subscribe((event: Event) => {
        console.log(event);
        this.event = event
      } )
  }

  goBack(): void
  {
    this.location.back();
  }

  onDelete(): void
  {
    this.httpService.delete(this.detail.eventId)
      .subscribe(_ => this.goBack())
  }
  onSave(): void
  {
    this.httpService.update(this.detail)
      .subscribe(() => this.goBack())
  }
}
