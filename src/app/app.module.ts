import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from "@angular/material/icon"
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from "@angular/material/toolbar"
import { MatCardModule } from "@angular/material/card"
import { MatMomentDateModule } from '@angular/material-moment-adapter'
import { MatRadioModule } from '@angular/material/radio';

import { AgGridModule } from 'ag-grid-angular';
import { MomentModule } from 'ngx-moment';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { EventSearchComponent } from './event-search/event-search.component';
import { EventsComponent } from './events/events.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { ConfigHttpComponent } from './http/config/config.http.component';
import { EventDetailUrlComponent } from './state/eventDetail/eventDetail.url.component';
import { EventUrlComponent } from './state/event/event.url.component';
import { DateWeekComponent } from './date-week/date-week.component';
import { DateQuarterComponent } from './date-quarter/date-quarter.component';
import { DateMonthComponent } from './date-month/date-month.component';

@NgModule({
  declarations: [
    AppComponent,
    MessagesComponent,
    DashboardComponent,
    EventSearchComponent,
    EventUrlComponent,
    EventsComponent,
    EventDetailComponent,
    ConfigHttpComponent,
    EventDetailUrlComponent,
    DateWeekComponent,
    DateQuarterComponent,
    DateMonthComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,

    BrowserAnimationsModule,

    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatToolbarModule,
    MatCardModule,
    MatMomentDateModule,
    MatRadioModule,

    AgGridModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
