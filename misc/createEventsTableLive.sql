create table payments.event
(
    _id      int auto_increment
        primary key,
    event_date    date      not null,
    event_type    char(50)  not null,
    event_summary char(255) not null
);


