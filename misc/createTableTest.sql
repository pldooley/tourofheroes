create table payment.test
(
    id      int auto_increment
        primary key,
    date    date      not null,
    type    char(50)  not null,
    summary char(255) not null,
    size    int       null,
    details char(255) null
);

create index test_date_index
    on payment.test (date);
